
const authController = require("../controller/auth.controller");
const { getAllStudents } = require("../controller/student.controller");
const router = require("express").Router();
const { verifyToken } = require('../middleware/verifyToken.middleware')


router.post('/auth/register',  authController.register )
router.post('/auth/login', authController.login)
router.get('/student/all', verifyToken,  getAllStudents)

module.exports = router