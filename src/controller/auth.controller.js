const jwt = require("jsonwebtoken");
const { registerService, loginService } = require("../services/auth.services");

const register = async (req, res) => {
  try {
    const { email, username, password } = req.body;
    const newUser = await registerService(email, password, username);
    const user = await newUser.save();
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json(error);
  }
};

const login = async (req, res) =>{
	try {
		const {email, password } = req.body
		const user = await loginService(email, password)
    
    const accessToken = jwt.sign(
      {
        id: user._id,
        username: user.username
      },
      process.env.JWT_SEC,
      { expiresIn: "3d" }
    );
    
    
    res.status(200).json({user, accessToken })
	} catch (error) {
		res.status(500).json(error)
	}
}

module.exports = {
  register,
  login
};
