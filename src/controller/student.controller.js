const services = require('../services/student.service')

const getAllStudents = async(req, res) =>{
	try {
		const student = await services.studentServices()
		res.status(200).json(student)
	} catch (error) {
		res.status(500).json(error)
	}
}

module.exports = {
	getAllStudents
}